import React, { Component } from 'react';
import { NavLink } from 'react-router-dom';
import './Draft.css';

class Draft extends Component {
	constructor(props) {
		super(props);
		this.state = {};
	}
	componentDidMount() {}
	render() {
		const { postsIsLoading, posts } = this.props;
		return (
			<div className="allposts container-fluid">
				<ul className={`content${postsIsLoading ? `is-loading` : ''}`}>
					{!postsIsLoading && posts.length > 0 ? (
						posts.map((post) => {
							if (post.status === 'draft') {
								return (
									<li key={post.id} className="post-item">
										<input type="checkbox" className="checkbox" />
										<div className="title">{post.title}
											<div className="title-options">
												<NavLink onClick={this.props.setNewPostTypeIsUpdate} 
												to={{
													pathname:"/newpost",
													state:{post}
													}}
												>Edit</NavLink>
											</div>
										</div>
										<div className="draft-mention">
											{post.status === 'draft' ? 'Draft' : '     '}
										</div>
										<div className="author-name">{post.author}</div>
										<div className="comment-count">{post.commentCount}</div>
										<div className="published">{post.published}</div>
									</li>
								);
							}
						})
					) : null}
				</ul>
				<div className="loader-icon">
					<i className="fa fa-spinner fa-spin" />
				</div>
			</div>
		);
	}
}

export default Draft;
