import React, { Component } from 'react';
import { Route, Redirect, NavLink } from 'react-router-dom';
import './MainComponents.css';

import AllPosts from './posts/all-posts/AllPosts';
import Draft from './posts/draft/Draft';
import Published from './posts/published/Published';

class MainComponents extends Component {
	//to add the activ class to accordian
	constructor(props) {
		super(props);
	}
	componentDidMount() {
		(function() {
			$('.panel').on('show.bs.collapse hide.bs.collapse', function(e) {
				if (e.type == 'show') {
					$(this).addClass('active');
				} else {
					$(this).removeClass('active');
				}
			});
		}.call(this));
	}
	render() {
		return (
			<React.Fragment>
				<div id="main-components-navigation">
					<div className="blog-description">
						<h3>My blog</h3>
						<a href="#">view blog</a>
					</div>
					<div className="col-md-3">
						<div className="container-fluid">
							<div className="panel-group" id="accordion">
								{/* all panels */}
								<div className="panel panel-default active">
									<NavLink
										to={`${this.props.match.path}/allposts`}
										data-toggle="collapse"
										data-parent="#accordion"
										data-target="#collapseOne"
										className="panel-navlink-heading"
									>
										<div className="panel-heading">
											<h4 className="panel-title">
												<span className="glyphicon glyphicon-folder-close" />Posts
											</h4>
										</div>
									</NavLink>
									<div id="collapseOne" className="panel-collapse collapse in">
										<ul className="panel-body">
											<li>
												<NavLink to={`${this.props.match.path}/allposts`}>AllPosts</NavLink>
											</li>

											<li>
												<NavLink to={`${this.props.match.path}/draft`}>Draft</NavLink>
											</li>

											<li>
												<NavLink to={`${this.props.match.path}/published`}>Published</NavLink>
											</li>
										</ul>
									</div>
								</div>
								<div className="panel panel-default">
									<NavLink
										to={`${this.props.match.path}/allposts`}
										data-toggle="collapse"
										data-parent="#accordion"
										data-target="#collapseTwo"
										className="panel-navlink-heading"
									>
										<div className="panel-heading">
											<h4 className="panel-title">
												<span className="glyphicon glyphicon-folder-close" />Stats
											</h4>
										</div>
									</NavLink>
									<div id="collapseTwo" className="panel-collapse collapse in">
										<ul className="panel-body">
											<li>
												<NavLink to={`${this.props.match.path}/allposts`}>AllPosts</NavLink>
											</li>

											<li>
												<NavLink to={`${this.props.match.path}/drafts`}>Draft</NavLink>
											</li>

											<li>
												<NavLink to={`${this.props.match.path}/published`}>Published</NavLink>
											</li>
										</ul>
									</div>
								</div>
								<div className="panel panel-default">
									<NavLink
										to={`${this.props.match.path}/allposts`}
										data-toggle="collapse"
										data-parent="#accordion"
										data-target="#collapseThree"
										className="panel-navlink-heading"
									>
										<div className="panel-heading">
											<h4 className="panel-title">
												<span className="glyphicon glyphicon-folder-close" />Comments
											</h4>
										</div>
									</NavLink>
									<div id="collapseThree" className="panel-collapse collapse in">
										<ul className="panel-body">
											<li>
												<NavLink to={`${this.props.match.path}/allposts`}>AllPosts</NavLink>
											</li>

											<li>
												<NavLink to={`${this.props.match.path}/drafts`}>Draft</NavLink>
											</li>

											<li>
												<NavLink to={`${this.props.match.path}/published`}>Published</NavLink>
											</li>
										</ul>
									</div>
								</div>
								<div className="panel panel-default">
									<NavLink
										to={`${this.props.match.path}/allposts`}
										data-toggle="collapse"
										data-parent="#accordion"
										data-target="#collapseFour"
										className="panel-navlink-heading"
									>
										<div className="panel-heading">
											<h4 className="panel-title">
												<span className="glyphicon glyphicon-folder-close" />Earnings
											</h4>
										</div>
									</NavLink>
									<div id="collapseFour" className="panel-collapse collapse in">
										<ul className="panel-body">
											<li>
												<NavLink to={`${this.props.match.path}/allposts`}>AllPosts</NavLink>
											</li>

											<li>
												<NavLink to={`${this.props.match.path}/draft`}>Draft</NavLink>
											</li>

											<li>
												<NavLink to={`${this.props.match.path}/published`}>Published</NavLink>
											</li>
										</ul>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>

				<form id="main-components-body-wrapper">
					<NavLink className="btn btn-primary" to="/newpost">
						New post
					</NavLink>
					<div id="posts">
						<div id="posts-heading">
							<a className="checkbox" />
							<a className="btn btn-info">publish</a>
							<a className="btn btn-info">Revert to draft</a>
						</div>
						<Route
							path={`${this.props.match.path}`}
							render={() => <Redirect to={`${this.props.match.path}/allposts`} />}
						/>
						<Route
							path={`${this.props.match.path}/allposts`}
							render={(props) => (
								<AllPosts
									{...props}
									posts={this.props.posts}
									postsIsLoading={this.props.postsIsLoading}
									setNewPostTypeIsUpdate={this.props.setNewPostTypeIsUpdate}
								/>
							)}
						/>
						<Route
							path={`${this.props.match.path}/draft`}
							render={(props) => (
								<Draft {...props} posts={this.props.posts} postsIsLoading={this.props.postsIsLoading} 
								setNewPostTypeIsUpdate={this.props.setNewPostTypeIsUpdate}/>
							)}
						/>
						<Route
							path={`${this.props.match.path}/published`}
							render={(props) => (
								<Published
									{...props}
									posts={this.props.posts}
									postsIsLoading={this.props.postsIsLoading}
									setNewPostTypeIsUpdate={this.props.setNewPostTypeIsUpdate}
								/>
							)}
						/>
					</div>
				</form>
			</React.Fragment>
		);
	}
}

export default MainComponents;
