import React, { Component } from 'react';
import { hot } from 'react-hot-loader';
import './header.css';
class Header extends Component {
	constructor(props) {
		super(props);
		this.state = {
			title: 'myBlogger'
		};
	}

	render() {
		return (
			<nav className="navbar navbar-default" role="navigation">
				<div className="container-fluid">
					<div className="navbar-header">
						<button
							type="button"
							className="navbar-toggle"
							data-toggle="collapse"
							data-target="#main-navbar"
						>
							<span className="sr-only">nav toggle</span>
							<span className="fa fa-bars" />
						</button>
						<div className="navbar-brand" title="myBlogger" onClick={() => this.changeTo('/allposts')}>
							<i className="fa fa-th-large" />
							<div className="name">{this.state.title}</div>
						</div>
					</div>
					<div className="collapse navbar-collapse" id="main-navbar">
						<ul className="nav nav-pills navbar-right">
							<li className="">
								<a>
									<i className="fa fa-th" />
								</a>
							</li>
							<li className="">
								<a>
									<i className="fa fa-bell" />
								</a>
							</li>
							<li className="current-user-icon">
								<a className="btn btn-ouline-primary">
									<i className="fa fa-bold" />
								</a>
							</li>
						</ul>
					</div>
				</div>
			</nav>
		);
	}
}

export default hot(module)(Header);
