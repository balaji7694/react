import React, { Component } from "react";
import { hot } from "react-hot-loader";
import { BrowserRouter, Route, Switch, Redirect } from "react-router-dom";

import Header from "./header/Header";
import NewPost from "./new-post/Newpost";
import Error from "./error/Error";
import MainComponents from "./main-components/MainComponents";

class App extends Component {
	constructor(props) {
		super(props);
		this.state = {
			posts: [],
			postsIsLoading: true,
			newPostTypeIsUpdate: false
		};
		this.addItemToPosts = this.addItemToPosts.bind(this);
		this.preparePost = this.preparePost.bind(this);
		this.generateuid = this.generateuid.bind(this);
		this.updateItemInPosts =this.updateItemInPosts.bind(this);
		this.setNewPostTypeIsUpdate=this.setNewPostTypeIsUpdate.bind(this);
		this.setNewPostTypeIsPost = this.setNewPostTypeIsPost.bind(this)
	}
	componentDidMount() {
		fetch("http://localhost:8080/sampData.json")
			.then((res) => res.json())
			.then((json) =>
				json.posts.items.map(({ id, title, author, published, replies, status, content }) => ({
					id,
					title,
					author: author.displayName,
					commentCount: replies.totalItems,
					status,
					published: new Date(published).toDateString().substr(4),
					content
				}))
			)
			.then((posts) => {
				this.setState({
					posts: posts,
					postsIsLoading: false
				});
			})
			.catch((err) => console.log(err));
	}
	render() {
		return (
			<BrowserRouter>
				<React.Fragment>
					<Header />
					<Switch>
						<Route exact path="/" render={() => <Redirect to="/maincomponents" />} />
						<Route
							path="/maincomponents"
							render={(props) => (
								<MainComponents
									{...props}
									posts={this.state.posts}
									postsIsLoading={this.state.postsIsLoading}
									setNewPostTypeIsUpdate={this.setNewPostTypeIsUpdate}
								/>
							)}
						/>
						<Route
							path="/newpost"
							render={(props) => <NewPost {...props} postItem={this.addItemToPosts} updateItem={this.updateItemInPosts} newPostTypeIsUpdate={this.state.newPostTypeIsUpdate} setNewPostTypeIsPost={this.setNewPostTypeIsPost}/>}
						/>
					</Switch>
				</React.Fragment>
			</BrowserRouter>
		);
	}
	setNewPostTypeIsUpdate(){
		let newPostTypeIsUpdate = true;
		this.setState({newPostTypeIsUpdate});
	}
	setNewPostTypeIsPost(){
		let newPostTypeIsUpdate = false;
		this.setState({newPostTypeIsUpdate});
	}
	updateItemInPosts(title, content, id){
		let { posts } = this.state;
		const newposts = posts.map((post)=> {
			if(post.id !== id){
				return post
			}
			post.title = title;
			post.content = content;
			return post;
		});
		this.setState({ posts: newposts,
			newPostTypeIsUpdate: false  }, () => {
		});
	}
	addItemToPosts(title, content) {
		const post = this.preparePost(title, content);
		let { posts } = this.state;
		const newposts = [post, ...posts]
		this.setState({ posts: newposts }, () => {
		});
	}
	preparePost(title, content) {
		let post = {
			author: "Brett Wiltshire",
			commentCount: "0",
			id: this.generateuid(),
			published: "Aug 02 2011",
			status: "live",
			title,
			content
		};
		return post;
	}

	generateuid() {
		function s4() {
			return Math.floor((1 + Math.random()) * 0x10000).toString(16).substring(1);
		}
		return s4() + s4() + "-" + s4() + "-" + s4() + "-" + s4() + "-" + s4() + s4() + s4();
	}
}

export default App;
