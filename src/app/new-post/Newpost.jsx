import React, { Component } from 'react';
import { hot } from 'react-hot-loader';
import './Newpost.css';

import TopHolder from './top-holder/TopHolder';
import Editor from './editor/Editor';
import PostSettings from './post-settings/PostSettings';
class NewPost extends Component {
	constructor(props) {
		super(props);
		this.state = {
			id: '',
			title: '',
			content: ''
		};
		this.setContent = this.setContent.bind(this);
		this.setTitle = this.setTitle.bind(this);
		this.callPostItem = this.callPostItem.bind(this);
		this.callUpdateItem = this.callUpdateItem.bind(this);
	}
	componentDidMount(){
		if(this.props.location.state){
			let {id, title, content} = this.props.location.state.post
			this.setState({id, title,content})
		}
		else{
			this.props.setNewPostTypeIsPost();
		}
	}
	render() {
		const {id} = this.props.match.params
		return (
			<form method="post">
				<TopHolder callPostItem={this.callPostItem} setTitle={this.setTitle} title={this.state.title} callUpdateItem={this.callUpdateItem} isUpdate={this.props.newPostTypeIsUpdate}/>
				<Editor setContent={this.setContent} content={this.state.content} />
				{/* <PostSettings className="post-settings-area" /> */}
			</form>
		);
	}
	setContent(evt) {
		let content = evt.target.value;
		this.setState({ content });
		evt.target.value = '';
	}
	setTitle(evt) {
		let title = evt.target.value;
		this.setState({ title });
		evt.target.value = '';
	}
	callPostItem() {
		this.props.postItem(this.state.title, this.state.content);
	}
	callUpdateItem() {
		this.props.updateItem(this.state.title, this.state.content, this.state.id);
	}
}

export default hot(module)(NewPost);
