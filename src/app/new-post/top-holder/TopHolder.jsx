import React, { Component } from 'react';
import { NavLink } from 'react-router-dom';
import './TopHolder.css';

class TopHolder extends Component {
	render() {
		return (
			<div className="top-holder">
				<h2 className="post-title">
					<NavLink to="/">My blog</NavLink>
					<span>.Post</span>
				</h2>
				<input
					type="text"
					className=""
					placeholder="Post title"
					onChange={this.props.setTitle}
					value={this.props.title}
				/>
				<span className="post-buttons">
					<span className="post-as">
						<span className="icon">
							<i className="fa fa-user-circle" />
						</span>
						<span className="name">
							Posting as <b>Balaji</b>
						</span>
					</span>
					
					<NavLink onClick={this.props.isUpdate? this.props.callUpdateItem: this.props.callPostItem} to="/" className="btn blogg-button blogg-primary" >
					{this.props.isUpdate? "Update": "Publish"}
					</NavLink>
					<NavLink to="/" className="btn blogg-button">
						Save
					</NavLink>
					<NavLink to="/" className="btn blogg-button">
						Preview
					</NavLink>
					<NavLink to="/" className="btn blogg-button">
						Close
					</NavLink>
				</span>
			</div>
		);
	}
}

export default TopHolder;
