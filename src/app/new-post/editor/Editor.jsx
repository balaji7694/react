import React, { Component } from 'react';
import './Editor.css';
class Body extends Component {
	render() {
		return (
			<div className="editor-holder">
				<div className="editor-tools">
					<h2>Tools Here!</h2>
				</div>
				<div className="main-editor">
					<textarea
						name="editor"
						id="main-edtor"
						cols="80"
						rows="20"
						className="text-content"
						onChange={this.props.setContent}
						value={this.props.content}
					/>
				</div>
			</div>
		);
	}
}

export default Body;
