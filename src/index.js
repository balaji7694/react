import React from 'react';
import ReactDOM from 'react-dom';
import {createStore} from 'redux';
import {Provider} from 'react-redux';
import './index.css';

import App from './app/App';
import reducer from './allReducers';

const store = createStore(reducer, "MainStore")

ReactDOM.render(  <Provider store={store}><App /></Provider>, document.getElementById('app'));
