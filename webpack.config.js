const path = require('path');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const webpack = require('webpack');
module.exports = {
	entry: './src/index.js',
	output: { path: path.join(__dirname, 'dist'), filename: 'bundle.js', publicPath: '/' },
	module: {
		rules: [
			{
				test: /\.(js|jsx)$/,
				exclude: /node_modules/,
				use: [ 'babel-loader' ]
			},
			{
				test: /\.css$/,
				use: [ { loader: 'style-loader' }, { loader: 'css-loader' } ]
			}
		]
	},
	devServer: {
		historyApiFallback: true,
		host: 'localhost',
		publicPath: '/'
	},
	resolve: {
		extensions: [ '*', '.js', '.jsx' ]
	},
	plugins: [ new HtmlWebpackPlugin({ template: './src/index.html' }) ]
};
